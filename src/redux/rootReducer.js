import { combineReducers } from 'redux'
import currencies from './modules/currencies'
import description from './modules/description'
import users from './modules/users'

export default combineReducers({
    currencies,
    description,
    users
});

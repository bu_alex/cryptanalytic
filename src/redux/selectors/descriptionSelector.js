export const descriptionSelector = state => {
    return state.description.data
}

export const isFetchingSelector = state => state.description.dataFetching

import _ from 'lodash'

export const userOneSelector = state => {
    return _.clone(state.users.dataUserAuth)
}
export const isUserAuthFetchingSelector = state => state.users.dataUserAuthFetching
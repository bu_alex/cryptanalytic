export const searchSelector = state => state.currencies.data;
export const filteredDataSelector = state => state.currencies.filteredData;

export const isFetchingSelector = state => !!state.currencies;
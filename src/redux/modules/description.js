import { handleActions } from 'redux-actions'
import { resolve, request, reject } from 'redux-promised'
import { GET_ONE_COIN, NAMESPACE } from "./const"

// ------------------------------------
// Constants
// ------------------------------------
export const GET_DESCRIPTION = `${NAMESPACE}/GET_DESCRIPTION`

// ------------------------------------
// Actions
// ------------------------------------
const getDescription = (id_crypto) => {
    return{
        type: GET_DESCRIPTION,
        meta: { promise: true },
        payload: fetch(`${GET_ONE_COIN}${id_crypto}?expand=1`, {
            method: 'GET',
            mode:'cors'
        }).then(response=>response.json())
    }
}

export const actions = {
    getDescription,
}

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
    [resolve(GET_DESCRIPTION)]: (state, { payload }) => ({...state, data:payload, dataFethcing:false}),
    [reject(GET_DESCRIPTION)]: (state, { payload }) => ({...state, data:null, dataFethcing:false}),
    [request(GET_DESCRIPTION)]: (state, { payload }) => ({...state, data:null, dataFethcing:true})
},{reversed:false})


import { createAction, handleActions } from 'redux-actions'
import { resolve, request, reject } from 'redux-promised'

import { BASE_URL, ADD_NEW_USER, SET_USER_DATA, CHECK_AUTORIZATION, USER_LOGOUT } from './const'

// ------------------------------------
// Actions
// ------------------------------------
const setUserData = createAction(SET_USER_DATA);

const logout = createAction(USER_LOGOUT);
const addNewUser = (data) => {
    var status = 1;
    let dataString = 'username=' + data[0] + '&password=' + data[1] + '&status=' + status;
    return {
        type: ADD_NEW_USER,
        meta: { promise: true },
        payload: postUserData(BASE_URL + 'users/add', dataString)
            .then(data => JSON.stringify(data)) // JSON-string from `response.json()` call
            .catch(error => console.error(error))
    }
}
const checkAutorization = (authParams) => {
    let dataString = 'username=' + authParams[0] + '&password=' + authParams[1]
    return {
        type: CHECK_AUTORIZATION,
        meta: { promise: true },
        payload: postUserData(BASE_URL + 'users/auth', dataString)
    }
}
function postUserData(url, bodydataString) { //This function only for JSON data sending !!!
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: bodydataString, // body data type must match "Content-Type" header
    })
    .then(response => response.clone().json() // parses response to JSON
    )
    .catch(error => console.error(error))
};

export const actions = {
    addNewUser,
    checkAutorization,
    logout
}
export const setUserDataIfExist = {
    setUserData
}

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
// ADD_NEW_USER
    [resolve(ADD_NEW_USER)]: (state, { payload }) => {
        return{...state, dataNewUser:payload, dataNewUserFetching:false}
    },
    [reject(ADD_NEW_USER)]: (state, { payload }) => {
        return{...state, dataNewUser:payload, dataNewUserFetching:false}
    },
    [request(ADD_NEW_USER)]: state => {
        return{...state, dataNewUser:null, dataNewUserFetching:true}
    },

// SET_USER_DATA
    [SET_USER_DATA]: (state, { payload }) => {
        return{...state, dataUserAuth:payload, dataUserAuthFetching:false};
    },

// CHECK_AUTORIZATION
    [resolve(CHECK_AUTORIZATION)]: (state, { payload }) => {
        localStorage.setItem('uuid', JSON.stringify(payload))
        return{...state, dataUserAuth:payload, dataUserAuthFetching:false};
    },
    [reject(CHECK_AUTORIZATION)]: (state, { payload }) => {
        return{...state, dataUserAuth:payload, dataUserAuthFetching:false}
    },
    [request(CHECK_AUTORIZATION)]: state => {
        return{...state, dataUserAuth:null, dataUserAuthFetching:true}
    },
// USER_LOGOUT
    [USER_LOGOUT]: state => {
        localStorage.removeItem('uuid')
        return{...state, dataUserAuth:null, dataUserAuthFetching:false}
    }
},{reversed:false})

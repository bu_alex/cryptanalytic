import { createAction, handleActions } from 'redux-actions'
import { resolve, request, reject } from 'redux-promised'
import { filter } from "lodash"
import axios from 'axios'
import {ADD_COIN, COINS_URL, NAMESPACE, UPDATE_COIN} from "./const"

// ------------------------------------
// Constants
// ------------------------------------
export const GET_CURRENCY = `${NAMESPACE}/GET_CURRENCY`
export const ADD_CURRENCY = `${NAMESPACE}/ADD_CURRENCY`
export const UPDATE_CURRENCY = `${NAMESPACE}/UPDATE_CURRENCY`
export const SET_SEARCH_FILTER = `${NAMESPACE}/SET_SEARCH_FILTER`

// ------------------------------------
// Actions
// ------------------------------------
const getCurrency = () => {
    return{
        type: GET_CURRENCY,
        meta: { promise: true },
        payload: fetch(COINS_URL,{
            method: 'GET',
            mode:'cors'
        }).then(response=>response.json())
    }
}

const setSearchFilter  = createAction(SET_SEARCH_FILTER);

const addCurrency = (params) =>
{
    return{
        type: ADD_CURRENCY,
        meta: { promise: true },
        payload: axios.post(ADD_COIN, params)
            .then(res => {
            }).catch(function(error) {
            })
    }
}

const updateCurrency = (putParams,symbol) =>{
    debugger;
    return{
        type:UPDATE_CURRENCY,
        meta: { promise: true },
        payload :axios.post(`${UPDATE_COIN}${symbol}`,putParams)
            .then(res => {
            }).catch(function(error) {
            })
    }
}

export const actions = {
    getCurrency,
    addCurrency,
}

export const searchActions = {
    setSearchFilter
};

export const update_action = {
    updateCurrency
}

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
    [resolve(GET_CURRENCY)]: (state, { payload }) => ({...state, data:payload, dataFethcing:false}),
    [reject(GET_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:false}),
    [request(GET_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:true}),

    [resolve(ADD_CURRENCY)]: (state, { payload }) => ({...state, data:payload, dataFethcing:false}),
    [reject(ADD_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:false}),
    [request(ADD_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:true}),

    [resolve(UPDATE_CURRENCY)]: (state, { payload }) => ({...state, data:payload, dataFethcing:false}),
    [reject(UPDATE_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:false}),
    [request(UPDATE_CURRENCY)]: (state, { payload }) => ({...state, data:null, dataFethcing:true}),

    [SET_SEARCH_FILTER]: (state, { payload }) => {
        const { data = [] } = state.data || {};
        let filteredData = null;
        if(payload){
            const searchTest = new RegExp('\\b'+payload,'i');
            filteredData = filter(data,function(o){
                return searchTest.test(o.name) || searchTest.test(o.symbol);
            })
        }
        return {...state, filteredData};
    },
},{reversed:false})


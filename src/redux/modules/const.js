export const BASE_URL  = 'http://km.softbistro.online:20080/'

export const COINS_URL = `${BASE_URL}coins?expand=1`
export const ADD_COIN = `${BASE_URL}coins/add`
export const GET_ONE_COIN = `${BASE_URL}coins/get/`
export const UPDATE_COIN = `${BASE_URL}coins/update/`


export const NAMESPACE = 'currencies'

export const USER_NAMESPACE = 'users'
export const ADD_NEW_USER = `${USER_NAMESPACE}/ADD_NEW_USER`
export const SET_USER_DATA = `${USER_NAMESPACE}/SET_USER_DATA`
export const CHECK_AUTORIZATION = `${USER_NAMESPACE}/CHECK_AUTORIZATION`
export const USER_LOGOUT  = `${USER_NAMESPACE}/USER_LOGOUT`
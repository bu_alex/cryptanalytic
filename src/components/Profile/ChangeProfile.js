import React, {Component} from 'react';
import {connect} from 'react-redux'
import {userOneSelector} from "../../redux/selectors/userSelector";

import '../../App.css'

class ChangeProfile extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center align-items-center">
                    <div className="generalForm">
                        <h3 className="text-center">Редагування</h3>
                        <hr></hr>
                        <form>
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">Нове ім'я користувача</label>
                                <input type="text" className="form-control" id="formGroupExampleInput"
                                       placeholder="Введіть ім'я"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput2">Новий пароль</label>
                                <input type="text" className="form-control" id="formGroupExampleInput2"
                                       placeholder="Введіть пароль"/>
                            </div>
                        </form>
                        <button type="button" className="btn btn-danger btn-lg btn-block">Змінити</button>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    userOne: userOneSelector(state)
});

export default connect(mapStateToProps)(ChangeProfile);
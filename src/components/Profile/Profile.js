import React, {Component} from 'react';
import {connect} from 'react-redux'
import {NavLink} from "react-router-dom";
import {userOneSelector} from "../../redux/selectors/userSelector";

import style from "../Description/Description.module.css";
import './Profile.css'

class Profile extends Component {
    uuid;
    date_create;

    render() {
        const {userOne} = this.props;

        if (userOne && userOne.data && userOne.data[0]) {
            return (
                <div className="container">
                    <div className="userOneData">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="card card-profile">
                                    <div className="card-avata">
                                        <a href="#" className="profile_img">
                                            <img className="aw"
                                                 src="https://codeseller.ru/wp-content/uploads/2017/09/%D0%91%D0%B5%D0%B7-%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-1.png"/>
                                        </a>
                                    </div>
                                    <div className="card-content">
                                        <h6 className="badge badge-pill badge-success">Ваш профиль</h6>
                                        <div className="list-group">
                                            <h4 className="card-title">Имя {userOne.data[0].username}!</h4>
                                            <p className="description">Информация о себе
                                                <div><strong>Ваш id:</strong> {userOne.data[0].id}</div>
                                                <div><strong>Ваш uuid:</strong> {userOne.data[0].uuid}</div>
                                                <div><strong>Дата регистрации:</strong> {userOne.data[0].date_create}
                                                </div>
                                            </p>
                                            <NavLink to={'/profile_change'} className={`${style.link_back}`}>
                                                <button type="button"
                                                        className={`btn btn-warning btn-block btn-lg  ${style.link_position}`}>Редагувати
                                                </button>
                                            </NavLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav>
                                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                       href="#nav-home" role="tab" aria-controls="nav-home"
                                       aria-selected="true">Home</a>
                                    <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                       href="#nav-profile" role="tab" aria-controls="nav-profile"
                                       aria-selected="false">Profile</a>
                                    <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                       href="#nav-contact" role="tab" aria-controls="nav-contact"
                                       aria-selected="false">Contact</a>
                                </div>
                            </nav>
                            <div className="tab-content" id="nav-tabContent">
                                <div className="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                </div>
                                <div className="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">...
                                </div>
                                <div className="tab-pane fade" id="nav-contact" role="tabpanel"
                                     aria-labelledby="nav-contact-tab">...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return null;
    }
}

const mapStateToProps = (state) => ({
    userOne: userOneSelector(state)
});

export default connect(mapStateToProps)(Profile);
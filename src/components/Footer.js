import React, { Component } from 'react'

import 'bootstrap/dist/css/bootstrap.css'
import '../App.css'

class Footer extends Component{
    render(){
        return(
            <div className="bg-dark">
                <footer className="page-footer font-small blue pt-4 end panel-primary">
                    <div className="container-fluid text-center text-md-left">
                        <div className="row">
                            <div className="col-md-6 mt-md-0 mt-3">
                                <h5 className="text-white">Footer Content</h5>
                                <p className = "text-white">Here you can use rows and columns here to organize your footer content.</p>
                            </div>
                        </div>
                    </div>
                    <div className="footer-copyright text-center py-3 text-white"> © 2019 Copyright
                        <a href="https://mdbootstrap.com/education/bootstrap/"> </a>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer

import React, { Component } from 'react'
import { connect } from "react-redux"
import { actions } from "../../redux/modules/currencies"
import { currencySelector, isFetchingSelector } from "../../redux/selectors/currencySelector"
import '../../App.css'
import 'bootstrap/dist/css/bootstrap.css'

class Add extends Component {
    state = {
        symbol: '',
        name: '',
        status: '',
        description: ''
    }

    handleChange = (event) =>
    {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = event =>
    {
        event.preventDefault();
        var params = new URLSearchParams();
        params.append('symbol', this.state.symbol);
        params.append('name', this.state.name);
        params.append('status', this.state.status);
        params.append('description', this.state.description);
        this.props.addCurrency(params);
    }

    renderAdd(){
        return(

                <div className="container">
                    <div className="row justify-content-center align-items-center">
                        <div className="generalForm">
                            <h3 className="text-center">Додавання нової крипто валюти</h3>
                            <hr/>
                            <div className="form-group">
                                <label htmlFor="InputName">Назва</label>
                                <input
                                    value={this.state.name}
                                    name="name"
                                    onChange={this.handleChange}
                                    type="text"
                                    className="form-control"
                                    id="InputLogin2"
                                />
                                <label htmlFor="InputSymbol">Скорочена назва</label>
                                <input
                                    value={this.state.symbol}
                                    name="symbol"
                                    onChange={this.handleChange}
                                    type="text"
                                    className="form-control"
                                    id="InputSymbol"
                                />
                                <label htmlFor="InputStatus">Статус</label>
                                <input
                                    value={this.state.status}
                                    name="status"
                                    onChange={this.handleChange}
                                    type="text"
                                    className="form-control"
                                    id="InputStatus"
                                />
                                <label htmlFor="InputDescription">Опис</label>
                                <input
                                    value={this.state.description}
                                    name="description"
                                    onChange={this.handleChange}
                                    type="text"
                                    className="form-control"
                                    id="InputDescription"
                                />
                            </div>
                            <button type="button" className="btn btn-success btn-lg btn-block " onClick={this.handleSubmit}>Додати криптовалюту</button>
                        </div>
                    </div>
                </div>

        )
    }

    render()
    {
        return( this.renderAdd() )
    }
}
const mapDispatchToProps = {
    ...actions
}

const mapStateToProps = (state) => ({
    curencies:currencySelector(state),
    isFetching:isFetchingSelector(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(Add)

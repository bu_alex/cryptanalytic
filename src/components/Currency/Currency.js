import React, {Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { actions } from '../../redux/modules/currencies'
import { currencySelector, isFetchingSelector } from '../../redux/selectors/currencySelector'
import { filteredDataSelector } from '../../redux/selectors/searchSelector'
import { NavLink } from "react-router-dom"
import { map } from 'lodash'
import TableLine from "../TableLine"
import Loading from "../Loading"

import style from './Currency.module.css'
import 'bootstrap/dist/css/bootstrap.css'
import '../../App.css'

class Currency extends Component {
    componentDidMount(){
        this.props.getCurrency()
    }

    renderCoins(){
        const {curencies, isFetching, filteredData} = this.props
        const coins = map(filteredData || curencies.data, curency =>
            <TableLine key={curency.id} {...curency} change = {curency.coin_info.percent_change_1h}/>
        )

        return(
            <Fragment>
                <div className={`${style.posText} ${style.text}`}>
                    Топ криптовалют
                </div>
                <div className="container">
                    <div className="row mainheader" >
                        <div className="col-md-2">Символ</div>
                        <div className="col-md-2">Назва</div>
                        <div className="col-md-2">Ціна</div>
                        <div className="col-md-2">Зміни</div>
                        <div className="col-md-4">
                            <NavLink className="add_crypto" to={`/add`} >
                                <button type="button" className="btn btn-success btn-block">Додати нову криптовалюту</button>
                            </NavLink>
                        </div>
                    </div>
                </div>
                <div>{ coins }</div>
            </Fragment>
        )
    }

    render() {
        const { curencies, isFetching } = this.props
        return curencies && !isFetching ? this.renderCoins() : <Loading/>
    }
}

const mapDispatchToProps = {
    ...actions
}

const mapStateToProps = (state) => ({
    curencies:currencySelector(state),
    filteredData:filteredDataSelector(state),
    isFetching:isFetchingSelector(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(Currency)

import React, { Component } from 'react'
import { connect } from "react-redux"
import { actions } from "../../redux/modules/description"
import {update_action} from "../../redux/modules/currencies";
import {descriptionSelector,isFetchingSelector} from "../../redux/selectors/descriptionSelector";
import '../../App.css'
import 'bootstrap/dist/css/bootstrap.css'
import Loading from "../Loading";

class Edit extends Component {

    constructor(props) {
        super(props);
        console.log(props)
        const { description = {} } = props;
        const { data = {} }  = description;
        this.state = {
            symbol: data[0] && data[0].symbol ? data[0].symbol : "",
            name: data[0] && data[0].name ? data[0].name : "",
            status: data[0] && data[0].status ? data[0].status : "",
            description: data[0] && data[0].description ? data[0].description : "",
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    setDescriptionData(props){
        const { description = {} } = props;
        const { data = {} }  = description || {};
        this.setState({
            symbol: data[0] && data[0].symbol ? data[0].symbol : "",
            name: data[0] && data[0].name ? data[0].name : "",
            status: data[0] && data[0].status ? data[0].status : "",
            description: data[0] && data[0].description ? data[0].description : "",
        });
    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.setDescriptionData(nextProps);
    }

    componentDidMount(){
        if (!this.state.symbol){
            this.props.getDescription(this.props.match.params.symbol);
        }
    }

    handleChange = (event) =>
    {
        this.setState({
            [event.target.name]: event.target.value

        });
    };

    handleSubmit = event => {
        event.preventDefault();
        var putParams = new URLSearchParams();
        putParams.append('symbol', this.state.symbol);
        putParams.append('name', this.state.name);
        putParams.append('status', this.state.status);
        putParams.append('description', this.state.description);
        this.props.updateCurrency(putParams, this.state.symbol);
    };

    renderDescription(){
        const state = this.state;
        return(<div>
            <div className="container">
                <div  className="row justify-content-center align-items-center">
                    <div className="generalForm">
                        <h3 className="text-center">Редагування </h3>
                        <hr/>
                        <div className="form-group">
                            <label htmlFor="InputName">Назва</label>
                            <input
                                value={state.name}
                                name="name"
                                onChange={this.handleChange}
                                type="text"
                                className="form-control"
                                id="InputLogin2"
                            />
                            <label htmlFor="InputSymbol">Скорочена назва</label>
                            <input
                                value={state.symbol}
                                name="symbol"
                                onChange={this.handleChange}
                                type="text"
                                className="form-control"
                                id="InputSymbol"
                            />
                            <label htmlFor="InputStatus">Статус</label>
                            <input
                                value={state.status}
                                name="status"
                                onChange={this.handleChange}
                                type="text"
                                className="form-control"
                                id="InputStatus"
                            />
                            <label htmlFor="InputDescription">Опис</label>
                            <input
                                value={state.description}
                                name="description"
                                onChange={this.handleChange}
                                type="text"
                                className="form-control"
                                id="InputDescription"
                            />
                        </div>
                        <button type="button" className="btn btn-success btn-lg btn-block " onClick={this.handleSubmit}>Редагувати</button>
                    </div>
                </div>
            </div>
        </div> )}

    render() {
        const { description, isFetching } = this.props;
        return description && !isFetching ? this.renderDescription() : <Loading/>
    }
}
const mapDispatchToProps = {
    ...actions,
    ...update_action
};

const mapStateToProps = (state) => {
    return {
        description:descriptionSelector(state),
        isFetching:isFetchingSelector(state),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isFetchingSelector, searchSelector } from "../../redux/selectors/searchSelector"
import { searchActions } from "../../redux/modules/currencies"

import style from './Head.module.css'

class Search extends Component{
    constructor(props) {
        super(props);
    }

    handleChange(e){
        this.props.setSearchFilter(e.currentTarget.value)
    }

    render() {
        return (
            <div className={style.d5}>
                <form>
                    <input type="text" placeholder="Искать здесь..." onChange={this.handleChange.bind(this)}/>
                </form>
            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    ...searchSelector(state),
    dataFetching: isFetchingSelector(state)
});

const mapDispatchToProps = {
    ...searchActions
};

export default connect(mapStateToProps,mapDispatchToProps)(Search);

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navbar, Nav } from 'react-bootstrap'
import { NavLink } from "react-router-dom"
import { setUserDataIfExist } from '../../redux/modules/users'

import Search from "./Search"
import Identification from "./Identification"

import '../../App.css'
import style from './Head.module.css'

class Head extends Component{
    componentWillMount(){
        this.checkUserData()
    }
    componentWillUpdate(){
        this.checkUserData()
    }
    checkUserData = () => {      
        if(localStorage.uuid)
        {
            let LoggedInUserData = JSON.parse(localStorage.uuid)
            this.props.setUserData(LoggedInUserData)
        }
    }
    renderHead(){
        return(
            <header>
                <Navbar expand="lg" bg="dark" variant="dark">
                    <NavLink to='/home'><Navbar.Brand>React-Crypto</Navbar.Brand></NavLink>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <div className={`${style.item} ${style.activeLink}`}>
                                <NavLink to='/home' activeClassName={style.activeLink}>Головна</NavLink>
                            </div>
                            <div className={style.item}>
                                <NavLink to='/subscription' activeClassName={style.activeLink}>Підписки</NavLink>
                            </div>
                        </Nav>
                        <Nav>
                            <Identification/>
                        </Nav>
                        <Search/>
                    </Navbar.Collapse>
                </Navbar>
            </header>
        )
    }

    render() {
        return (
            this.renderHead()
        )
    }
}

const mapDispatchToProps = {
    ...setUserDataIfExist
}

export default connect(null, mapDispatchToProps)(Head);

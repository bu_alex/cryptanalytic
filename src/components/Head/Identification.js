import React, { Component } from 'react'
import { NavLink } from "react-router-dom"
import { connect } from 'react-redux'
import { actions } from '../../redux/modules/users'
import { userOneSelector } from '../../redux/selectors/userSelector'

import '../../App.css';
import style from './Head.module.css'

class IdentificationNavPart extends Component{
    handleClick = (e) => {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        const { userOne } = this.props;
        if(userOne)
            if(userOne.data[0])
                return(
                    <React.Fragment>
                        <div className={style.item}>
                            <NavLink to='/profile' activeClassName={style.activeLink}>Профіль</NavLink>
                        </div>
                        <div className={style.item}>
                            <a href="" onClick={this.handleClick.bind(this)}>
                                    Logout: {userOne.data[0].username}
                            </a>
                        </div>
                    </React.Fragment>
                )

        return(
            <React.Fragment>
                <div className={style.item}>
                        <NavLink to='/registration' activeClassName={style.activeLink}>Реєстрація</NavLink>
                </div>
                <div className={style.item}>
                        <NavLink to='/authorization' activeClassName={style.activeLink}>Авторизація</NavLink>
                </div>
            </React.Fragment>
        )
    }
}


const mapDispatchToProps = {
    ...actions
}

const mapStateToProps = (state) => ({
    userOne: userOneSelector(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(IdentificationNavPart);
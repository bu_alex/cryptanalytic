import React, { Component } from 'react'

class  Loading extends Component{
    render() {
        return (
            <div className="row container" id="fountainTextG">
                <div id="fountainTextG_1" className="fountainTextGT">З</div>
                <div id="fountainTextG_2" className="fountainTextGT">а</div>
                <div id="fountainTextG_3" className="fountainTextGT">г</div>
                <div id="fountainTextG_4" className="fountainTextGT">р</div>
                <div id="fountainTextG_5" className="fountainTextGT">у</div>
                <div id="fountainTextG_6" className="fountainTextGT">з</div>
                <div id="fountainTextG_7" className="fountainTextGT">к</div>
                <div id="fountainTextG_8" className="fountainTextGT">а</div>
            </div>
        )
    }
}

export default Loading
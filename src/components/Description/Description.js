import React, { Component } from 'react'
import {connect} from 'react-redux'
import { actions } from '../../redux/modules/description'
import {NavLink} from "react-router-dom"
import { descriptionSelector, isFetchingSelector } from '../../redux/selectors/descriptionSelector'
import { map } from 'lodash'
import Loading from "../Loading"
import style from './Description.module.css'

class Description extends Component {
    componentDidMount(){
        this.props.getDescription(this.props.match.params.symbol)
    }

    renderOneRow = (title,data)=> {
        return(
            <div className="row">
                <div className={`col-lg-6 col ${style.const_information}`}>{title}</div>
                <div className={`col-lg-6 col ${style.description}`}>{data}</div>
            </div>
        )
    }

    renderDescription(){
        const { description } = this.props
        return(
            <div>
                {
                    map(description.data, valuta =>
                        <div key={valuta.id} className="information container">
                            <div><h1 className="text-center">Загальний опис</h1></div>
                            {this.renderOneRow("Cкорочена назва:",valuta.symbol)}
                            {this.renderOneRow("Повна назва:", valuta.name)}
                            {this.renderOneRow("Ціна:", valuta.price)}
                            {this.renderOneRow("Опис:",valuta.description)}
                            {this.renderOneRow("Оборот валюти:",valuta.coin_info.volume_24h)}
                            {this.renderOneRow("Зміни за останню годину:",valuta.coin_info.percent_change_1h)}
                            {this.renderOneRow("Зміни за останній день:",valuta.coin_info.percent_change_24h)}
                            {this.renderOneRow("Зміни за останніх 7 днів:",valuta.coin_info.percent_change_7d)}
                            {this.renderOneRow("Капіталізація:",valuta.coin_info.market_cap)}
                            <div className="row block-">
                                <div className={`col-lg-12 center-block`}>
                                    <NavLink to={`/edit/${valuta.symbol}`} className={`${style.link_back}`}>
                                        <button type="button" className={`btn btn-warning btn-block btn-lg  ${style.link_position}`}>Редагувати</button>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="row">
                                <div className={`col-lg-12 col text-center ${style.link_position}`}>
                                    <NavLink to={'/home'} className={`${style.link_back}`}>Повернутись до ТОП
                                        Криптовалют</NavLink>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }

    render() {
        const { description, isFetching } = this.props;
        return description && !isFetching ? this.renderDescription() : <Loading/>
    }
}

const mapDispatchToProps = {
    ...actions
}

const mapStateToProps = (state) => ({
    description:descriptionSelector(state),
    isFetching:isFetchingSelector(state),
})

export default connect(mapStateToProps, mapDispatchToProps)(Description)
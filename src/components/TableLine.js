import React from 'react';
import {NavLink} from "react-router-dom";


let TableLine = (props) => {
    const {id, symbol, name, price, change} = props
    const image = change > 0 ? "green/arrow-up_green.png" : "red/arrow-down_red.png"
    return (
        <div className="container">
            <NavLink to={`/description/${symbol}`}>
                <div key={id} className="row information">
                    <div className="col-md-2 inf_text">{symbol}</div>
                    <div className="col-md-2 inf_text">{name}</div>
                    <div className="col-md-2 inf_text">{price}</div>
                    <div className="col-md-2"><img alt={image} src={`https://cdn0.iconfinder.com/data/icons/super-mono-reflection/${image}`}/></div>
                </div>
            </NavLink>
        </div>
    );
}

export default TableLine;
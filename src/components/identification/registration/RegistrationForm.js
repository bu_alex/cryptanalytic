import React, { Component } from 'react';
import { connect } from 'react-redux'
import { actions } from '../../../redux/modules/users'

import '../../../App.css'

class RegistrationForm extends Component {
    constructor() {
        super();
        this.state = {
            userName: '',
            password: ''
        }
    }
    handleChange = (event) =>
    {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    handleClick() {
        let data = [this.state.userName, this.state.password];
        this.props.addNewUser(data);

        this.setState({
            userName: '',
            password: ''
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center align-items-center">
                    <div className="generalForm">
                        <h3 className="text-center">Реєстрація</h3>
                        <hr></hr>
                        <div className="form-group">
                            <label htmlFor="userName">Ім'я</label>
                            <input 
                                value = {this.state.userName}
                                name = 'userName'
                                onChange = {this.handleChange}
                                type = "email" 
                                className = "form-control"
                                id = "userName"  
                                placeholder = "Enter first name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="InputPassword1">Пароль</label>
                            <input 
                                value = {this.state.password}
                                name = 'password'
                                onChange = {this.handleChange}
                                type = "password" 
                                className = "form-control" 
                                id = "InputPassword1"  
                                placeholder = "Enter your password"/>
                            <small id = "passwordHelp" className="form-text text-muted">Намагайтесь створити якомога складніший пароль.</small>
                        </div>
                            
                        <button type="button" className="btn btn-primary btn-lg btn-block" onClick={this.handleClick.bind(this)}>Зареєструвати мене!</button>
                    </div>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = {
    ...actions
}

export default connect(null, mapDispatchToProps)(RegistrationForm)

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { actions } from '../../../redux/modules/currencies'
import { userOneSelector } from '../../../redux/selectors/userSelector'
import AutForm from "./AutorizationForm"

import '../../../App.css'

class AutorizationPage extends Component {   
    render() {
        const { userOne } = this.props;
        if(!userOne){
            return (
                <AutForm/>
            )}
        else {
            if(userOne.data[0])
                return(
                    <div className="userOneData">
                        <h2>Вы успешно авторизировались {userOne.data[0].username}!</h2>
                        <div><strong>Ваш статус:</strong> {userOne.data[0].status}</div>
                        <div><strong>Ваш uuid:</strong> {userOne.data[0].uuid}</div>
                        <div><strong>Дата регистрации:</strong> {userOne.data[0].date_create}</div>
                    </div>
                )
            
            else {
            // there we have unsigned user because of errors
            // all errors(false password or unexist login)
            // would be writed there after same form of autorization
                return(
                    <AutForm/>
                )
            }
        }
    }
}

  
const mapDispatchToProps = {
    ...actions
}

const mapStateToProps = (state) => ({
    userOne: userOneSelector(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(AutorizationPage)

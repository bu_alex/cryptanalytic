import React, { Component } from 'react';
import { connect } from 'react-redux'
import { actions } from '../../../redux/modules/users'

import '../../../App.css'

class AutorizationForm extends Component {
    constructor() {
        super();
        this.state={
          userName: '',
          password: ''
        }
      }
      
      handleChange = (event) =>
      {
        this.setState({
          [event.target.name]: event.target.value
        });
      }
      handleClick = (event) => {
        let data = [this.state.userName, this.state.password];
        this.props.checkAutorization(data);
      }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center align-items-center">
                    <div className="generalForm">
                        <h3 className="text-center">Авторизація</h3>
                        <hr></hr>
                        <div className="form-group">
                            <label htmlFor="InputLogin2">Логін</label>
                            <input
                                value={this.state.userName}
                                name="userName"
                                onChange={this.handleChange} 
                                type="email" 
                                className="form-control" 
                                id="InputLogin2" 
                                aria-describedby="emailHelp" 
                                placeholder="Enter your login"/>
                            <label htmlFor="InputPassword2">Пароль</label>
                            <input
                                value={this.state.password}
                                name="password"
                                onChange={this.handleChange} 
                                type="password" 
                                className="form-control" 
                                id="InputPassword2" 
                                aria-describedby="passwordHelp" 
                                placeholder="Enter your password"/>
                        </div> 
                        <button type="button" className="btn btn-primary btn-lg btn-block" onClick={this.handleClick.bind(this)}>Увійти</button>
                    </div>
                </div>
            </div>
        )
    }
}

  
const mapDispatchToProps = {
    ...actions
}

export default connect(null, mapDispatchToProps)(AutorizationForm)

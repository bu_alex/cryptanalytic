import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch } from "react-router-dom"
import configureStore from './redux/configureStore'

import Header from './components/Head/Head'
import Footer from './components/Footer'
import Description from "./components/Description/Description"
import Currency from './components/Currency/Currency'
import Add from "./components/Currency/Add"
import Edit from "./components/Currency/Edit";
import RegistrationForm from './components/identification/registration/RegistrationForm';
import AutorizationForm from './components/identification/authorization/AutorizationPage';
import Profile from "./components/Profile/Profile";
import ChangeProfile from "./components/Profile/ChangeProfile";

import './App.css'

const store = configureStore();

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <div>
            <Header/>
            <Switch>
              <Route exact path = '/'               component = { Currency }/>
              <Route path = "/home"                 component = { Currency }/>
              <Route path = '/description/:symbol'  component = { Description }/>
              <Route path = "/add"                  component = { Add }/>
              <Route path = '/registration'         component = { RegistrationForm }/>
              <Route path = '/authorization'        component = { AutorizationForm }/>
              <Route path = '/profile'              component = { Profile }/>
              <Route path = '/profile_change'       component = { ChangeProfile }/>
              <Route path = '/edit/:symbol'         component = { Edit }/>
            </Switch>
            <Footer/>
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
